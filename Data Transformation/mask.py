import numpy as np
import os
import cv2
import pandas as pd


class card_dataset():
    def __init__(self,filepath,json_file,output_mask_path):
        """Here, filepath : file path of image folder,
                json_file : file path of MS-COCO JSON file,
                output_mask_path : file path for saving output mask file. """
        self._filepath = filepath
        self._json_file = json_file
        self._output_mask_path = output_mask_path

    """Generating training images as well mask images for training"""
    def load_dataset(self):
       """_mask image function_"""
       def load_mask(bbox,width,height,class_id):
           box = bbox
           w = width
           h = height
           masks = np.zeros([h, w, 1], dtype='uint8')
           # create mask
           class_ids = class_id
           if len(box)==4:
               x1 = int(box[0])
               x2 = int(box[1])
               x3 = int(box[2])
               x4 = int(box[3])
               masks[x2:x4, x1:x3, 0] = 255
           else:
               (x1, y1) = bbox[0]
               (x2, y2) = bbox[1]
               (x3, y3) = bbox[2]
               (x4, y4) = bbox[3]
               xmin = min(x1, x2, x3, x4)
               ymin = min(y1, y2, y3, y4)
               xmax = max(x1, x2, x3, x4)
               ymax = max(y1, y2, y3, y4)
               masks[int(ymin):int(ymax), int(xmin):int(xmax)]= 255
           return masks, np.asarray(class_ids, dtype='int32')

       """_load training image_"""
       def train_img(image_path):
           img = cv2.imread(image_path)
           n_img = np.array(img)
           return n_img

       """_load image reference_"""
       def image_reference(image_id):
           info = image_id
           return info

       """_Reading json file as database_"""
       df = pd.read_json(self._json_file)
       image_info = df['images'][0]['id']
       for i in range(len(df)):
           bbox = df['annotations'][i]['bbox']
           width = int(df['images'][i]['width'])
           height = int(df['images'][i]['height'])
           class_id = df['categories'][i]['label_id']
           [mask_img, class_id_n] = load_mask(bbox,width,height,class_id)
           save_name = 'mask_'+df['images'][i]['file_name'].split('.')[0]+'_'+str(i)+'.jpg'
           path = os.path.join(self._output_mask_path,save_name)
           cv2.imwrite(path, mask_img)

       return 0


if __name__=='__main__':
    card_dataset(filepath=r'D:\Users\deepali_vasundharaa\card\card_dataset\train',
        json_file = r'D:\Users\deepali_vasundharaa\card\card_dataset\rectangular_train_labels.json',
        output_mask_path = r'D:\Users\deepali_vasundharaa\card\card_dataset\mask').load_dataset()

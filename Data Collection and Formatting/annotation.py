import pandas as pd
import os
import cv2
import numpy as np

"""Function for converting the annotation from rectangular bounding box to oriented bounding box system."""
def annotate(img_path, annotation):
    img = cv2.imread(img_path)
    # crop the image according to annotation
    [xmin, ymin, xmax, ymax] = annotation
    crop_img = img[ymin:ymax, xmin:xmax]
    # find the contour on crop image
    gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (3, 3), 0)
    thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    contours = cv2.findContours(thresh.astype('uint8').copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    c = max(contours, key=cv2.contourArea)
    # four extreme points
    left = tuple(c[c[:, :, 0].argmin()][0])
    right = tuple(c[c[:, :, 0].argmax()][0])
    top = tuple(c[c[:, :, 1].argmin()][0])
    bottom = tuple(c[c[:, :, 1].argmax()][0])
    # converting it into pixel wise location
    l = (left[0] + xmin, left[1] + ymin)
    r = (right[0] + xmin, right[1] + ymin)
    t = (top[0] + xmin, top[1] + ymin)
    b = (bottom[0] + xmin, bottom[1] + ymin)
    (x1, y1) = l
    (x2, y2) = r
    (x3, y3) = t
    (x4, y4) = b
    annotate = [x1, y1, x2, y2, x3, y3, x4, y4]
    return annotate

if __name__=='__annotation__':
    main()

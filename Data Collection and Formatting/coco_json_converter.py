import pandas as pd
import os
import cv2
import numpy as np
import json
from annotation import annotate

"""Function for converting .csv file to coco_json format"""
def convert(csv_path, json_path):
    df = pd.read_csv(csv_path)
    json_dict = {"info": "Playing Card training dataset", "images": [], "type": "instances",
                 "annotations": [],
                 "categories": []}
    image_name = df['filename']
    h = df['height']
    w = df['width']
    image_id = 0
    # image info

    for i in range(len(df)):
        image_id += 1
        filename = image_name[i]
        height = h[i]
        width = w[i]
        image_info = {
            'file_name': filename,
            'height': np.float(height),
            'width': np.float(width),
            'id': int(image_id)
        }
        json_dict['images'].append(image_info)
    # annotations
    image_id = 0
    x1 = df['xmin']
    x2 = df['ymin']
    x3 = df['xmax']
    x4 = df['ymax']
    c = df['class']
    for i in range(len(df)):
        image_id += 1
        xmin = np.float(x1[i])
        ymin = np.float(x2[i])
        xmax = np.float(x3[i])
        ymax = np.float(x4[i])
        o_width = w[i]
        o_height = h[i]
        category_id = c[i]
        if c[i] == 'ace':
            category_id_no = 0
        elif c[i] == 'two':
            category_id_no = 1
        elif c[i] == 'three':
            category_id_no = 3
        elif c[i] == 'four':
            category_id_no = 4
        elif c[i] == 'five':
            category_id_no = 5
        elif c[i] == 'six':
            category_id_no = 6
        elif c[i] == 'seven':
            category_id_no = 7
        elif c[i] == 'eight':
            category_id_no = 8
        elif c[i] == 'nine':
            category_id_no = 9
        elif c[i] == 'ten':
            category_id_no = 10
        elif c[i] == 'jack':
            category_id_no = 11
        elif c[i] == 'queen':
            category_id_no = 12
        elif c[i] == 'king':
            category_id_no = 13

        ann = {
            'id': int(image_id),
            'image_id' : int(image_id),
            'area': np.float(o_width * o_height),
            'iscrowd': 0,
            'bbox': [xmin, ymin, xmax, ymax],
            'category_id': category_id_no,
            'ignore': 0,
            'segmentation': [[int(xmin), int(ymin), int(xmax), int(ymax)]]
        }
        json_dict['annotations'].append(ann)

    # category
    image_id = 0
    for i in range(len(df)):
        image_id += 1
        category_id = c[i]
        if c[i] == 'ace':
            category_id_no = 0
        elif c[i] == 'two':
            category_id_no = 1
        elif c[i] == 'three':
            category_id_no = 3
        elif c[i] == 'four':
            category_id_no = 4
        elif c[i] == 'five':
            category_id_no = 5
        elif c[i] == 'six':
            category_id_no = 6
        elif c[i] == 'seven':
            category_id_no = 7
        elif c[i] == 'eight':
            category_id_no = 8
        elif c[i] == 'nine':
            category_id_no = 9
        elif c[i] == 'ten':
            category_id_no = 10
        elif c[i] == 'jack':
            category_id_no = 11
        elif c[i] == 'queen':
            category_id_no = 12
        elif c[i] == 'king':
            category_id_no = 13

        cat = {
            'id': int(image_id),
            'label_id': category_id_no,
            'name': category_id,
            'supercategory': 'none'
        }
        json_dict['categories'].append(cat)
    json_file = json_path
    json_fp = open(json_file, 'w', encoding='utf-8')
    json_str = json.dumps(json_dict)
    json_fp.write(json_str)
    json_fp.close()
    return 0

"""Function for converting oriented bbox annotations to coco_json format"""
def csv_file_to_coco_json(filepath,folder,json_path):
    df = pd.read_csv(filepath)
    num = len(df)
    filename = df['filename']
    # Converting this information into database & then into a .json file
    df = pd.read_csv(filepath)
    json_dict = {"info": "Playing Card training dataset", "images": [], "type": "instances",
                 "annotations": [],
                 "categories": []}
    image_name = df['filename']
    h = df['height']
    w = df['width']
    image_id = 0
    # image info
    for i in range(len(df)):
        image_id += 1
        filename = image_name[i]
        height = h[i]
        width = w[i]
        image_info = {
            'file_name': filename,
            'height': np.float(height),
            'width': np.float(width),
            'id': int(image_id)
        }
        json_dict['images'].append(image_info)
    # annotations
    c = df['class']
    # calling annotation function for converting the annotation from rectangular bbox to oriented bbox annotation
    image_id = 0
    for i in range(len(df)):
        image_id += 1
        annotation = [df['xmin'][i], df['ymin'][i], df['xmax'][i], df['ymax'][i]]
        [x1, y1, x2, y2, x3, y3, x4, y4] = annotate(os.path.join(folder, df['filename'][i]),
                                                    annotation)
        x1 = np.float(x1)
        y1 = np.float(y1)
        x2 = np.float(x2)
        y2 = np.float(y2)
        x3 = np.float(x3)
        y3 = np.float(y3)
        x4 = np.float(x4)
        y4 = np.float(y4)

        o_width = w[i]
        o_height = h[i]
        category_id = c[i]
        if c[i] == 'ace':
            category_id_no = 0
        elif c[i] == 'two':
            category_id_no = 1
        elif c[i] == 'three':
            category_id_no = 3
        elif c[i] == 'four':
            category_id_no = 4
        elif c[i] == 'five':
            category_id_no = 5
        elif c[i] == 'six':
            category_id_no = 6
        elif c[i] == 'seven':
            category_id_no = 7
        elif c[i] == 'eight':
            category_id_no = 8
        elif c[i] == 'nine':
            category_id_no = 9
        elif c[i] == 'ten':
            category_id_no = 10
        elif c[i] == 'jack':
            category_id_no = 11
        elif c[i] == 'queen':
            category_id_no = 12
        elif c[i] == 'king':
            category_id_no = 13

        ann = {
            'id': int(image_id),
            'image_id': int(image_id),
            'area': np.float(o_width * o_height),
            'iscrowd': 0,
            'bbox': [(x1, y1),
                     (x2, y2),
                     (x3, y3),
                     (x4, y4)],
            'category_id': category_id_no,
            'ignore': 0,
            'segmentation': [[int(x1), int(y1),
                     int(x2), int(y2),
                     int(x3), int(y3),
                     int(x4), int(y4)]]
        }
        json_dict['annotations'].append(ann)

    # category
    image_id = 0
    for i in range(len(df)):
        image_id += 1
        category_id = c[i]
        if c[i] == 'ace':
            category_id_no = 0
        elif c[i] == 'two':
            category_id_no = 1
        elif c[i] == 'three':
            category_id_no = 3
        elif c[i] == 'four':
            category_id_no = 4
        elif c[i] == 'five':
            category_id_no = 5
        elif c[i] == 'six':
            category_id_no = 6
        elif c[i] == 'seven':
            category_id_no = 7
        elif c[i] == 'eight':
            category_id_no = 8
        elif c[i] == 'nine':
            category_id_no = 9
        elif c[i] == 'ten':
            category_id_no = 10
        elif c[i] == 'jack':
            category_id_no = 11
        elif c[i] == 'queen':
            category_id_no = 12
        elif c[i] == 'king':
            category_id_no = 13

        cat = {
            'id': int(image_id),
            'label_id': category_id_no,
            'name': category_id,
            'supercategory': 'none'
        }
        json_dict['categories'].append(cat)
    json_file = json_path
    json_fp = open(json_file, 'w', encoding='utf-8')
    json_str = json.dumps(json_dict)
    json_fp.write(json_str)
    json_fp.close()
    return 0


if __name__=='__coco_json_converter__':
    main()
from annotation import annotate
from coco_json_converter import convert, csv_file_to_coco_json


if __name__ == '__main__':
    """Annotate from rectangular bounding box to oriented bounding box system & convert that annotation file 
    into MS-COCO json format"""
    folder = r'D:\Users\deepali_vasundharaa\card\card_dataset\train' #training folder path
    csv_file = r'D:\Users\deepali_vasundharaa\card\card_dataset\train_labels.csv' #training label csv file path
    json_path = r'D:\Users\deepali_vasundharaa\card\card_dataset\oriented_train_labels.json' #output path for json file
    csv_file_to_coco_json(filepath=csv_file,folder=folder,json_path=json_path)
    """Convert rectangular csv training labels into MS-COCO json format"""
    rectangular_json_path = r'D:\Users\deepali_vasundharaa\card\card_dataset\rectangular_train_labels.json' #output path for json file
    convert(csv_path=csv_file,json_path=rectangular_json_path)
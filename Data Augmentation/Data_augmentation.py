import cv2
import numpy as np
import pandas as pd
import json
import os
from skimage.transform import rotate, AffineTransform, warp
from skimage import io
from skimage.util import img_as_ubyte

class DataAugmentation():
    def __init__(self,filepath,json_file,output_json_path,output_folder):
        """Here, filepath : file path of image folder,
                json_file : file path of MS-COCO JSON file,
                output_json_path : file path for saving output json file,
                output_folder : file path for saving the Augmented image. """
        self._filepath = filepath
        self._json_file = json_file
        self._output_json_path = output_json_path
        self._output_folder = output_folder

    """Augment image & generate , object oriented modified json file"""
    def augment_img(self):
        json_file = pd.read_json(self._json_file)
        json_dict = {"info": "Playing Card training dataset", "images": [], "type": "instances",
                     "annotations": [],
                     "categories": []
                     }
        filepath = self._filepath
        output_folder = self._output_folder
        image_id = 0
        """__annotation_function__"""
        def annotate(img_path):
            img = cv2.imread(img_path)
            # find the contour on crop image
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (3, 3), 0)
            thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
            contours = cv2.findContours(thresh.astype('uint8').copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = contours[0] if len(contours) == 2 else contours[1]
            c = max(contours, key=cv2.contourArea)
            # four extreme points
            left = tuple(c[c[:, :, 0].argmin()][0])
            right = tuple(c[c[:, :, 0].argmax()][0])
            top = tuple(c[c[:, :, 1].argmin()][0])
            bottom = tuple(c[c[:, :, 1].argmax()][0])
            # converting it into pixel wise location
            l = (left[0], left[1])
            r = (right[0], right[1])
            t = (top[0], top[1])
            b = (bottom[0], bottom[1])
            (x1, y1) = l
            (x2, y2) = r
            (x3, y3) = t
            (x4, y4) = b
            annotation = [np.float(x1), np.float(y1), np.float(x2), np.float(y2), np.float(x3), np.float(y3),np.float(x4),np.float(y4)]
            return annotation
        for i in range(len(json_file)):
            image_id += 1
            name = json_file['images'][i]['file_name']
            img = cv2.imread(os.path.join(filepath, name))
            bbox = json_file['annotations'][i]['bbox']
            (x1, y1) = bbox[0]
            (x2, y2) = bbox[1]
            (x3, y3) = bbox[2]
            (x4, y4) = bbox[3]
            xmin = min(x1, x2, x3, x4)
            ymin = min(y1, y2, y3, y4)
            xmax = max(x1, x2, x3, x4)
            ymax = max(y1, y2, y3, y4)
            crop_img = img[int(ymin):int(ymax), int(xmin):int(xmax)]
            name = name.split('.')[0]
            save_name = name +'_'+ str(i) + '_.jpg'
            filename_1 = os.path.join(output_folder, save_name)
            cv2.imwrite(filename_1, crop_img)
            annotation = annotate(filename_1)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = crop_img.shape[0]
            o_height = crop_img.shape[1]
            """_image_info_"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            category_id_no = json_file['annotations'][i]['category_id']
            """_annotation_info_"""
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """_Categories_"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """########################Rotation#####################################"""
            """_Rotation=90_"""
            image_id += 1
            image = cv2.rotate(crop_img, cv2.cv2.ROTATE_90_CLOCKWISE)
            save_name = name.split('.')[0] + '_rotate_90_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(image)
            cv2.imwrite(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = image.shape[0]
            o_height = image.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            category_id_no = json_file['annotations'][i]['category_id']
            """Annotation"""
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """_Rotation=45_"""
            image_id += 1
            image = rotate(crop_img, angle=45)
            save_name = name.split('.')[0] + '_rotate_45_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(image)
            io.imsave(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = image.shape[0]
            o_height = image.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'  # This script is not for segmentation
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """##############################Transformation###############################"""
            """Horizontally shift -10"""
            image_id += 1
            transform = AffineTransform(translation=(-20, 0))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'horizontal_-10_shift_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(tr_img)
            io.imsave(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """_Image_info_"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """horizontally +10 shift"""
            image_id += 1
            transform = AffineTransform(translation=(20, 0))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'horizonatl_+10_shift_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(tr_img)
            io.imsave(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """Vertically shift -10"""
            image_id += 1
            transform = AffineTransform(translation=(0, -20))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'vert_-10_shift_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(tr_img)
            io.imsave(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """Vertically shift +10"""
            image_id += 1
            transform = AffineTransform(translation=(0, 20))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'vert_+10_shift_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            image = img_as_ubyte(tr_img)
            io.imsave(filename_2, image)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """############################Brightness#####################################"""
            def increase_brightness(img, value):
                hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
                h, s, v = cv2.split(hsv)
                lim = 255 - value
                v[v > lim] = 255
                v[v <= lim] += value
                final_hsv = cv2.merge((h, s, v))
                img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
                return img

            bright_img = increase_brightness(crop_img, value=50)
            name = name.split('.')[0]
            save_name = name + 'bright_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            io.imsave(filename_2, bright_img)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = bright_img.shape[0]
            o_height = bright_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """#########################Sharpen##################################"""
            kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
            sharpen = cv2.filter2D(crop_img, -1, kernel)
            name = name.split('.')[0]
            save_name = name + 'sharpen_' + str(i) + '_.jpg'
            filename_2 = os.path.join(output_folder, save_name)
            io.imsave(filename_2, sharpen)
            annotation = annotate(filename_2)
            [x1, y1, x2, y2, x3, y3, x4, y4] = annotation
            o_width = bright_img.shape[0]
            o_height = bright_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': save_name,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [(x1, y1),
                         (x2, y2),
                         (x3, y3),
                         (x4, y4)],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)


        json_file = self._output_json_path
        json_fp = open(json_file, 'w', encoding='utf-8')
        json_str = json.dumps(json_dict)
        json_fp.write(json_str)
        json_fp.close()
        return 0

    """Augment image & generate , rectangular modified json file"""
    def rectangular_augment_img(self):
        json_file = pd.read_json(self._json_file)
        json_dict = {"info": "Playing Card training dataset", "images": [], "type": "instances",
                     "annotations": [],
                     "categories": []
                     }
        filepath = self._filepath
        output_folder = self._output_folder
        image_id = 0
        """__annotation_function__"""

        def annotate(img):
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(gray, (3, 3), 0)
            thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
            contours = cv2.findContours(thresh.astype('uint8').copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = contours[0] if len(contours) == 2 else contours[1]
            c = max(contours, key=cv2.contourArea)
            # four extreme points
            left = tuple(c[c[:, :, 0].argmin()][0])
            right = tuple(c[c[:, :, 0].argmax()][0])
            top = tuple(c[c[:, :, 1].argmin()][0])
            bottom = tuple(c[c[:, :, 1].argmax()][0])
            # converting it into pixel wise location
            l = (left[0], left[1])
            r = (right[0], right[1])
            t = (top[0], top[1])
            b = (bottom[0], bottom[1])
            (x1, y1) = l
            (x2, y2) = r
            (x3, y3) = t
            (x4, y4) = b
            xmin = min(x1, x2, x3, x4)
            ymin = min(y1, y2, y3, y4)
            xmax = max(x1, x2, x3, x4)
            ymax = max(y1, y2, y3, y4)
            annotation = [np.float(xmin), np.float(ymin), np.float(xmax), np.float(ymax)]
            return annotation

        for i in range(len(json_file)):
            image_id += 1
            name = json_file['images'][i]['file_name']
            img = cv2.imread(os.path.join(filepath, name))
            bbox = json_file['annotations'][i]['bbox']
            xmin = bbox[0]
            ymin = bbox[1]
            xmax = bbox[2]
            ymax = bbox[3]
            crop_img = img[int(ymin):int(ymax), int(xmin):int(xmax)]
            name = name.split('.')[0]
            save_name = name + '_' + str(i) + '_.jpg'
            filename_1 = save_name
            annotation = annotate(crop_img)
            [x1, y1, x2, y2] = annotation
            o_width = crop_img.shape[0]
            o_height = crop_img.shape[1]
            """_image_info_"""
            image_info = {
                'file_name': filename_1,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            category_id_no = json_file['annotations'][i]['category_id']
            """_annotation_info_"""
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """_Categories_"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """########################Rotation#####################################"""
            """_Rotation=90_"""
            image_id += 1
            image = cv2.rotate(crop_img, cv2.cv2.ROTATE_90_CLOCKWISE)
            save_name = name.split('.')[0] + '_rotate_90_' + str(i) + '_.jpg'
            filename_1 = save_name
            image = img_as_ubyte(image)
            annotation = annotate(image)
            [x1, y1, x2, y2] = annotation
            o_width = image.shape[0]
            o_height = image.shape[1]
            image_info = {
                'file_name': filename_1,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            # annotation
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """_Rotation=45_"""
            image_id += 1
            image = rotate(crop_img, angle=45)
            save_name = name.split('.')[0] + '_rotate_45_' + str(i) + '_.jpg'
            filename_2 = save_name
            image = img_as_ubyte(image)
            annotation = annotate(image)
            [x1, y1, x2, y2] = annotation
            o_width = image.shape[0]
            o_height = image.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """##############################Transformation###############################"""
            """Horizontally shift -10"""
            image_id += 1
            transform = AffineTransform(translation=(-20, 0))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'horizontal_-10_shift_' + str(i) + '_.jpg'
            filename_2 = save_name
            tr_img = img_as_ubyte(tr_img)
            annotation = annotate(tr_img)
            [x1, y1, x2, y2] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """_Image_info_"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """horizontally +10 shift"""
            image_id += 1
            transform = AffineTransform(translation=(20, 0))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'horizonatl_+10_shift_' + str(i) + '_.jpg'
            filename_2 = save_name
            tr_img = img_as_ubyte(tr_img)
            annotation = annotate(tr_img)
            [x1, y1, x2, y2] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2] ,
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """Vertically shift -10"""
            image_id += 1
            transform = AffineTransform(translation=(0, -20))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'vert_-10_shift_' + str(i) + '_.jpg'
            filename_2 = save_name
            tr_img = img_as_ubyte(tr_img)
            annotation = annotate(tr_img)
            [x1, y1, x2, y2] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """Vertically shift +10"""
            image_id += 1
            transform = AffineTransform(translation=(20, 0))
            tr_img = warp(crop_img, transform, mode='constant')
            name = name.split('.')[0]
            save_name = name + 'vert_+10_shift_' + str(i) + '_.jpg'
            filename_2 = save_name
            tr_img = img_as_ubyte(tr_img)
            annotation = annotate(tr_img)
            [x1, y1, x2, y2] = annotation
            o_width = tr_img.shape[0]
            o_height = tr_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """############################Brightness#####################################"""

            def increase_brightness(img, value):
                hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
                h, s, v = cv2.split(hsv)
                lim = 255 - value
                v[v > lim] = 255
                v[v <= lim] += value
                final_hsv = cv2.merge((h, s, v))
                img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
                return img

            bright_img = increase_brightness(crop_img, value=50)
            name = name.split('.')[0]
            save_name = name + 'bright_' + str(i) + '_.jpg'
            filename_2 = save_name
            annotation = annotate(bright_img)
            [x1, y1, x2, y2] = annotation
            o_width = bright_img.shape[0]
            o_height = bright_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2],
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)
            """#########################Sharpen##################################"""
            kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
            sharpen = cv2.filter2D(crop_img, -1, kernel)
            name = name.split('.')[0]
            save_name = name + 'sharpen_' + str(i) + '_.jpg'
            filename_2 = save_name
            annotation = annotate(sharpen)
            [x1, y1, x2, y2] = annotation
            o_width = bright_img.shape[0]
            o_height = bright_img.shape[1]
            """Image info"""
            image_info = {
                'file_name': filename_2,
                'height': np.float(o_height),
                'id': int(image_id),
                'width': np.float(o_width)
            }
            json_dict['images'].append(image_info)
            """annotation"""
            category_id_no = json_file['annotations'][i]['category_id']
            ann = {
                'area': np.float(o_width * o_height),
                'iscrowd': 0,
                'bbox': [x1, y1, x2, y2] ,
                'category_id': category_id_no,
                'ignore': 0,
                'segmentation': 'polygon'
            }
            json_dict['annotations'].append(ann)
            """Categories"""
            category_id = json_file['categories'][i]['name']
            cat = {
                'label_id': int(category_id_no),
                'name': category_id,
                'supercategory': 'none'
            }
            json_dict['categories'].append(cat)

        json_file = self._output_json_path
        json_fp = open(json_file, 'w', encoding='utf-8')
        json_str = json.dumps(json_dict)
        json_fp.write(json_str)
        json_fp.close()
        return 0


if __name__=='__main__':
    """Generating data augmentation files & oriented json file"""
    DataAugmentation(filepath=r'D:\Users\deepali_vasundharaa\card\card_dataset\train',
        json_file = r'D:\Users\deepali_vasundharaa\card\card_dataset\oriented_train_labels.json',
        output_json_path = r'D:\Users\deepali_vasundharaa\card\card_dataset\oriented_new_train_labels.json',
        output_folder = r'D:\Users\deepali_vasundharaa\card\card_dataset\try').augment_img()

    """Generating rectangular json file"""
    DataAugmentation(filepath=r'D:\Users\deepali_vasundharaa\card\card_dataset\train',
                     json_file=r'D:\Users\deepali_vasundharaa\card\card_dataset\rectangular_train_labels.json',
                     output_json_path=r'D:\Users\deepali_vasundharaa\card\card_dataset\rectangular_new_train_labels.json',
                     output_folder = None).rectangular_augment_img()